<?php

class Order extends CI_Controller
{

    public function index()
    {
        $result = $this->db->get("order")->result();
        $this->load->model("pagination_model");
        $this->load->library("pagination");
        $config["base_url"]=base_url("order");
        $config["per_page"]=2;
        $config["total_rows"]=$this->pagination_model->get_count_order();
        $config["num_links"]=2;
        $config["uri_segment"]=2;

        $this->pagination->initialize($config);
        $page=$this->uri->segment(2);
        $results=$this->pagination_model->get_records_order($config["per_page"],$page);
        $links=$this->pagination->create_links();
        $viewData = array(
            "result" => $result,
            "results"=>$results,
            "links"=>$links
        );
        $this->load->view("order_list", $viewData);
    }

    function fetch_product()
    {
        $this->load->model('dynamic_dependent_model');
        $supplier_id = $this->input->post("supplierId");
        $fetch = $this->dynamic_dependent_model->fetch_product($supplier_id);
        echo $fetch;

    }

    function fetch_order()
    {
        $this->load->model('dynamic_dependent_model');
        $product_id = $this->input->post("productId");
        $fetch = $this->dynamic_dependent_model->fetch_order($product_id);
        echo $fetch;

    }

    public function newPage()
    {


        $supplier = $this->db->where("isActive", 1)->get("supplier")->result();

        $viewData = array(
            "supplier" => $supplier
        );

        $this->load->view("order_add", $viewData);

    }

    public function save()
    {
        $id = $this->input->post("id");

        $invoice = $this->input->post("invoice");
        $product_id = $this->input->post("product_id");
        $supplier_id = $this->input->post("supplier_id");
        $quantity = $this->input->post("quantity");
        $new1 = $this->db->where("id", $id)->get("order")->row()->quantity - $quantity;

        $old_quantity = $this->db->where("id", $product_id)->get("product")->row()->quantity;

        $new_quantity = $old_quantity + $new1;


        $data = array(

            "quantity" => $new_quantity
        );

        $price = $this->input->post("price");
        $detail = $this->input->post("detail");
        $total_price = $quantity * $price;

        $viewData = array(
            "invoice" => $invoice,
            "product_id" => $product_id,
            "supplier_id" => $supplier_id,
            "quantity" => $quantity,
            "price" => $price,
            "date" => date("Y-m-d"),
            "detail" => $detail,
            "total_price" => $total_price,


        );
        if($new_quantity < 0){
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("noQuantity"),
                "alert-type" => "error"
            ));

            redirect(base_url("order/newPage"));
            die();



        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));
            $updatee = $this->db->where("id", $product_id)->update("product", $data);
            $insert = $this->db->insert("order", $viewData);

            redirect(base_url("order"));

        }

    }

    public function delete($id)
    {
        $delete = $this->db->where("id", $id)->delete("order");
        if ($delete) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"

            ));
            redirect(base_url("order"));
        } else {
            $this->session->set_userdata(array("alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"));

        }
    }

    public function updatePage($id, $sid)
    {

        $suppliers = $this->db->get("supplier")->result();
        $supplierss = $this->db->where("id", $sid)->get("supplier")->row()->id;

        $products = $this->db->where("supplier_id", $supplierss)->where("quantity >", 0)->get("product")->result();

        $result = $this->db->where("id", $id)->get("order")->row();
        $viewData = array(
            "result" => $result,
            "suppliers" => $suppliers,
            "products" => $products,
            "supplierss" => $supplierss
        );
        $this->load->view("order_edit", $viewData);

    }

    public function edit($id,$sid)
    {
        $invoice = $this->input->post("invoice");
        $supplier_id = $this->input->post("supplier_id");
        $product_id = $this->input->post("product_id");
        $quantity = $this->input->post("quantity");
        $new1 = $this->db->where("id", $id)->get("order")->row()->quantity - $quantity;

        $old_quantity = $this->db->where("id", $product_id)->get("product")->row()->quantity;

        $new_quantity = $old_quantity + $new1;


        $data = array(

            "quantity" => $new_quantity
        );

        $price = $this->input->post("price");
        $detail = $this->input->post("detail");
        $total_price = $quantity * $price;

        $viewData = array(
            "invoice" => $invoice,
            "supplier_id" => $supplier_id,
            "product_id" => $product_id,
            "quantity" => $quantity,
            "price" => $price,
            "detail" => $detail,
            "total_price" => $total_price,

        );



            if($new_quantity < 0){
                $this->session->set_userdata(array(
                    "alert" => true,
                    "alert-message" => getMessage("noQuantity"),
                    "alert-type" => "error"
                ));

                redirect(base_url("order/updatePage/$id/$sid"));
                die();



        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));
                $updatee = $this->db->where("id", $product_id)->update("product", $data);
                $update = $this->db->where("id", $id)->update("order", $viewData);

                redirect(base_url("order"));

            }

    }

}