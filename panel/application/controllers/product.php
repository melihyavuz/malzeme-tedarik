<?php

class Product extends CI_Controller
{
    public function index()
    {
        $this->load->model("pagination_model");
        $this->load->library("pagination");
        $config["base_url"]=base_url("product");
        $config["per_page"]=2;
        $config["num_links"]=1;
        $config["uri_segment"]=2;
        $config["total_rows"]=$this->pagination_model->get_count_product();
        $this->pagination->initialize($config);
        $page=$this->uri->segment(2);
        $results=$this->pagination_model->get_records_product($config["per_page"],$page);
        $links=$this->pagination->create_links();
$viewData=array(
    "results"=>$results,
    "links"=>$links
);


        $this->load->view("product_list", $viewData);
    }

    public function isActiveSetter()
    {
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "true") ? 1 : 0;
        $id = $this->input->post("id");
        $this->db->where(array("id" => $id))->update("product", array("isActive" => $isActive));
    }

    public function newPage()
    {
        $result = $this->db->where("isActive", 1)->get("category")->result();
        $supplier = $this->db->where("isActive", 1)->get("supplier")->result();

        $viewData = array("result" => $result, "supplier" => $supplier);
        $data = array(
            "supplier" => $supplier);
        $this->load->view("product_add", $viewData);

    }

    public function save()
    {
        $img_id = $_FILES["img_id"]["name"];

        $code = $this->input->post("code");
        $title = $this->input->post("title");
        $quantity = $this->input->post("quantity");

        $list = $this->input->post("list");
        $sale = $this->input->post("sale");
        $category = $this->input->post("category_id");
        $supplier = $this->input->post("supplier_id");
        $viewData = array("code" => $code,
            "title" => $title,
            "quantity" => $quantity,

            "list_price" => $list,
            "sale_price" => $sale,
            "category_id" => $category,
            "supplier_id" => $supplier,
            "img_id" => $img_id
        );
        $config['upload_path'] = 'uploads/product/';
        // $config['allowed_types']        = 'gif|jpg|png';
        $config['allowed_types'] = '*';

        //$config['max_size']             = 100;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 768;
        $this->load->library('upload', $config);
        $upload = $this->upload->do_upload('img_id');
        if (!$upload) {
            $this->session->set_userdata(
                array(
                    "alert" => true,
                    "alert-message" => getMessage("errorFileUpload"),
                    "alert-type" => "error"
                )
            );
            redirect(base_url("product/newPage"));
            die();
        } else {
            $this->session->set_userdata(
                array(
                    "alert" => true,
                    "alert-message" => getMessage("success"),
                    "alert-type" => "success"
                )
            );
        }
        $insert = $this->db->insert("product", $viewData);
        if ($insert) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"));
            redirect(base_url("product"));
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("error"),
                "alert-type" => "error"));
        }

    }

    public function delete($id)
    {
        $delete = $this->db->where("id", $id)->delete("product");
        if ($delete) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"));
            redirect(base_url("product"));
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("error"),
                "alert-type" => "error"));
        }
    }

    public function updatePage($id)
    {
        $result = $this->db->where("id", $id)->get("product")->row();
        $categories = $this->db->where("isActive", 1)->get("category")->result();
        $supplier = $this->db->where("isActive", 1)->get("supplier")->result();
        $category_id = $this->db->where("id", $id)->get("product")->row()->category_id;
        $supplier_id = $this->db->where("id", $id)->get("product")->row()->supplier_id;


        $viewData = array("result" => $result, "categories" => $categories, "supplier" => $supplier, "category_id" => $category_id, "supplier_id" => $supplier_id);
        $this->load->view("product_edit", $viewData);
    }

    public function edit($id)
    {
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "on") ? 1 : 0;
        $code = $this->input->post("code");
        $title = $this->input->post("title");
        $detail = $this->input->post("detail");
        $quantity = $this->input->post("quantity");
        $list = $this->input->post("list");
        $sale = $this->input->post("sale");
        $category_id = $this->input->post("category_id");
        $supplier_id = $this->input->post("supplier_id");
        $old_image = $_REQUEST["old_photo"];
        $img_id = $_FILES["img_id"]["name"];
        if ($img_id != "") {
            $config['upload_path'] = 'uploads/product/';
            // $config['allowed_types']        = 'gif|jpg|png';
            $config['allowed_types'] = '*';

            //$config['max_size']             = 100;
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            $this->load->library('upload', $config);
            $this->upload->do_upload('img_id');
        }
        if ($img_id == "") {
            $new_photo = $old_image;
        }
        $viewData = array(
            "isActive" => $isActive,
            "code" => $code,
            "title" => $title,
            "detail" => $detail,
            "quantity" => $quantity,
            "list_price" => $list,
            "sale_price" => $sale,
            "category_id" => $category_id,
            "supplier_id" => $supplier_id,
            "img_id" => $new_photo,

        );
        if (!empty($img_id)) {
            $viewData['img_id'] = $img_id;
        }
        $update = $this->db->where("id", $id)->update("product", $viewData);
        if ($update) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));
            redirect(base_url("product"));
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("error"),
                "alert-type" => "error"
            ));
        }

    }

}