<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("pagination_model");

        $this->load->library("pagination");
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /welcome.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $categories = $this->db->get("category")->result();
        $viewData = array(
            "categories" => $categories,
        );
        $this->load->view('category_list', $viewData);
    }

    public function index_ajax($offset = null)
    {
        $query='';
        if($this->input->post('query')){
            $query=$this->input->post('query');
        }

        $this->load->library('pagination');

        $limit = 10;
        $offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $config['base_url'] = base_url('category/index_ajax/');
        $config['total_rows'] = $this->pagination_model->get_records($limit, $offset,$query, $count = true);
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        $config['num_links'] = 3;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="" class="current_page">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data['categories'] = $this->pagination_model->get_records($limit, $offset,$query, $count = false);

        $data['pagelinks'] = $this->pagination->create_links();

        $this->load->view('category_list/index_ajax', $data);
    }

    public function isActiveSetter()
    {
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "true") ? 1 : 0;
        $id = $this->input->post("id");
        $data = array("isActive" => $isActive);

        $this->db->where("id", $id)->update("category", $data);
    }

    public function newPage()
    {
        $this->load->view("category_add");
    }

    public function save()
    {
        $title = trim(strip_tags($this->input->post("title")));
        $viewData = array("title" => $title);
        if ($viewData["title"] == "" || $viewData["title"] == " ") {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("blankField"),
                "alert-type" => "warning"));
            redirect(base_url("category/newPage"));
            die();

        }
        $save = $this->db->insert("category", $viewData);
        if ($save) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"));
            redirect(base_url("category"));
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("error"),
                "alert-type" => "error"));
        }
    }

    public function delete($id)
    {
        $product_id = $this->db->where("category_id", $id)->get("product")->row();
        if (!$product_id) {
            $delete = $this->db->where("id", $id)->delete("category");
            if ($delete) {
                $this->session->set_userdata(array(
                    "alert" => true,
                    "alert-message" => getMessage("success"),
                    "alert-type" => "success"));
                redirect(base_url("category"));
            } else {
                $this->session->set_userdata(array(
                    "alert" => true,
                    "alert-message" => getMessage("error"),
                    "alert-type" => "error"));
            }
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("notDeletedCategory"),
                "alert-type" => "error"));
        }
        redirect(base_url("category"));

    }

    public function editPage($id)
    {
        $row = $this->db->where("id", $id)->get("category")->row();
        $data = array("row" => $row);
        $this->load->view("category_edit", $data);
    }

    public function edit($id)
    {
        $title = $this->input->post("title");
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "on") ? 1 : 0;
        $data = array("title" => $title, "isActive" => $isActive);
        $update = $this->db->where("id", $id)->update("category", $data);
        if ($update) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"));
            redirect(base_url("category"));

        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("error"),
                "alert-type" => "error"));
        }
    }


}
