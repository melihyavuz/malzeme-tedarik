<?php

class Supplier extends CI_Controller
{
    public function index()
    {
        $result = $this->db->get("supplier")->result();
        $this->load->model("pagination_model");
        $this->load->library("pagination");
        $config["base_url"] = base_url("supplier");
        $config["total_rows"] = $this->pagination_model->get_count_supplier();
        $config["uri_segment"] = 2;
        $config["per_page"] = 2;
        $config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $results = $this->pagination_model->get_records_supplier($config["per_page"], $page);
        $links = $this->pagination->create_links();
        $viewData = array("result" => $result, "results" => $results, "links" => $links);
        $this->load->view("supplier_list", $viewData);
    }

    public function newPage()
    {
        $this->load->view("supplier_add");
    }

    public function save()
    {
        $title = $this->input->post("title");
        $address = $this->input->post("address");
        $phone = $this->input->post("phone");
        $mail = $this->input->post("mail");
        $viewData = array("title" => $title,
            "address" => $address,
            "phone" => $phone,
            "email" => $mail);
        $insert = $this->db->insert("supplier", $viewData);
        if ($insert) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"));
            redirect(base_url("supplier"));
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("error"),
                "alert-type" => "error"));
        }
    }

    public function delete($id)
    {
        $supplier_id = $this->db->where("supplier_id", $id)->get("product")->row();
        if (!$supplier_id) {
            $delete = $this->db->where("id", $id)->delete("supplier");

            if ($delete) {
                $this->session->set_userdata(array(
                    "alert" => true,
                    "alert-message" => getMessage("success"),
                    "alert-type" => "success"));
                redirect(base_url("supplier"));
            } else {
                $this->session->set_userdata(array(
                    "alert" => true,
                    "alert-message" => getMessage("error"),
                    "alert-type" => "error"));
            }
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("notSupplierDelete"),
                "alert-type" => "error"));
            redirect(base_url("supplier"));

        }
    }

    public function updatePage($id)
    {
        $result = $this->db->where("id", $id)->get("supplier")->row();
        $viewData = array("result" => $result);
        $this->load->view("supplier_edit", $viewData);
    }

    public function edit($id)
    {
        $title = $this->input->post("title");
        $address = $this->input->post("address");
        $phone = $this->input->post("phone");
        $mail = $this->input->post("mail");
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "on") ? 1 : 0;

        $viewData = array("title" => $title,
            "address" => $address,
            "phone" => $phone,
            "email" => $mail,
            "isActive" => $isActive);
        $update = $this->db->where("id", $id)->update("supplier", $viewData);
        if ($update) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"));
            redirect(base_url("supplier"));
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("error"),
                "alert-type" => "error"));
        }

    }

    public function isActiveSetter()
    {
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "true") ? 1 : 0;
        $id = $this->input->post("id");
        $data = array("isActive" => $isActive);

        $this->db->where("id", $id)->update("supplier", $data);
    }
}