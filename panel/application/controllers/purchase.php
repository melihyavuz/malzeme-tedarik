<?php

class Purchase extends CI_Controller
{
    public function index()
    {
        $result = $this->db->get("purchase")->result();
$this->load->model("pagination_model");
$this->load->library("pagination");
$config["base_url"]=base_url("purchase");
$config["per_page"]=2;
$config["num_links"]=2;
$config["total_rows"]=$this->pagination_model->get_count_purchase();
$config["uri_segment"]=2;
$this->pagination->initialize($config);
$page=$this->uri->segment(2);
$results=$this->pagination_model->get_records_purchase($config["per_page"],$page);
$links=$this->pagination->create_links();
        $viewData = array(
            "result" => $result,
            "results"=>$results,
            "links"=>$links
        );
        $this->load->view("purchase_list", $viewData);
    }

    public function fetch_purchase()
    {
        $this->load->model("dynamic_dependent_model");
        $supplier_id=$this->input->post("supplierId");

        echo $this->dynamic_dependent_model->fetch_purchase($supplier_id);

    }

    public function newPage()
    {

        $supplier = $this->db->where("isActive", 1)->get("supplier")->result();

        $viewData = array(
            "supplier" => $supplier,


        );

        $this->load->view("purchase_add", $viewData);
    }

    public function save()
    {
        $invoice = $this->input->post("invoice");
        $product_id = $this->input->post("product_id");
        $supplier_id = $this->input->post("supplier_id");
        $quantity = $this->input->post("quantity");
        $price = $this->input->post("price");
        $detail = $this->input->post("detail");
        $total_price = $quantity * $price;

        $viewData = array(
            "invoice" => $invoice,
            "product_id" => $product_id,
            "supplier_id" => $supplier_id,
            "quantity" => $quantity,
            "date" => date("Y-m-d"),
            "price" => $price,
            "detail" => $detail,
            "total_price" => $total_price,

        );
        $product = $this->db->where("id", $product_id)->get("product")->row();
        $old_quantity = $product->quantity;


        $new_quantity = $quantity + $old_quantity;
        $data = array(
            "quantity" => $new_quantity,
            "list_price" => $price

        );
        $update = $this->db->where("id", $product_id)->update("product", $data);

        $insert = $this->db->insert("purchase", $viewData);
        if ($insert) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));

            redirect(base_url("purchase"));
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("error"),
                "alert-type" => "error"

            ));
        }

    }

    public function delete($id)
    {
        $delete = $this->db->where("id", $id)->delete("purchase");
        if ($delete) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"

            ));
            redirect(base_url("purchase"));
        } else {
            $this->session->set_userdata(array("alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"));

        }
    }

    public function updatePage($id,$sid)
    {
        $suppliers = $this->db->get("supplier")->result();
        $supplierss = $this->db->where("id", $sid)->get("supplier")->row()->id;

        $products = $this->db->where("supplier_id", $supplierss)->where("isActive",1)->where("quantity >", 0)->get("product")->result();

        $result = $this->db->where("id", $id)->get("purchase")->row();
        $viewData = array(
            "result" => $result,
            "suppliers" => $suppliers,
            "products" => $products,
            "supplierss" => $supplierss
        );
        $this->load->view("purchase_edit", $viewData);
    }

    public function edit($id)
    {
        $invoice = $this->input->post("invoice");
        $product_id = $this->input->post("product_id");
        $supplier_id = $this->input->post("supplier_id");
        $quantity = $this->input->post("quantity");
        $price = $this->input->post("price");
        $detail = $this->input->post("detail");
        $total_price = $quantity * $price;

        $new1 = $this->db->where("id", $id)->get("purchase")->row()->quantity - $quantity;

        $old_quantity = $this->db->where("id", $product_id)->get("product")->row()->quantity;

        $new_quantity = $old_quantity - $new1;


        $data = array(
            "list_price" => $price,

            "quantity" => $new_quantity
        );


        $viewData = array(
            "invoice" => $invoice,
            "product_id" => $product_id,
            "supplier_id" => $supplier_id,
            "quantity" => $quantity,
            "price" => $price,
            "detail" => $detail,
            "total_price" => $total_price

        );
        $updatee = $this->db->where("id", $product_id)->update("product", $data);

        $update = $this->db->where("id", $id)->update("purchase", $viewData);
        if ($update) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"

            ));
            redirect(base_url("purchase"));
        } else {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("error"),
                "alert-type" => "error"

            ));
        }

    }

}