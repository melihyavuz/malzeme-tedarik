<?php

class Dynamic_dependent_model extends CI_Model
{


    function fetch_product($supplier_id)
    {
        $product = $this->db->where("supplier_id", $supplier_id)->where("isActive",1)->where("quantity >", 0)->get("product")->result();
        $output = '<option value="">Ürün Seçiniz...</option>';
        foreach ($product as $row) {
            $output .= '<option value="' . $row->id . '">' . $row->title . '</option>';
        }
        return $output;

    }


    function fetch_order($product_id)
    {
        $product = $this->db->where("id", $product_id)->get("product")->row()->sale_price;
        $output = ' <div class="control-group" id="price">
            <label class="control-label">Satış Fiyatı</label>
            <div class="controls" >
                <input type="text" class="span3 " value="'.$product.'" name="price" id="pricee" readonly="readonly" />
                <span class="help-inline">Satış Fiyatı Giriniz...</span>
            </div>
        </div>';
        return $output;


    }
    function fetch_purchase($supplier_id){
        $product = $this->db->where("supplier_id", $supplier_id)->where("isActive",1)->where("quantity >", 0)->get("product")->result();
        $output = '<option value="">Ürün Seçiniz...</option>';
        foreach ($product as $row) {
            $output .= '<option value="' . $row->id . '">' . $row->title . '</option>';
        }
        return $output;
    }
}