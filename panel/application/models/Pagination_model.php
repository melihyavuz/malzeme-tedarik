<?php

class Pagination_model extends CI_Model
{
    public function get_records($limit, $offset, $query, $count = true)
    {
        $this->db->select('*');
        $this->db->from('category');
        if ($query != '') {
            $this->db->like('title', $query);

        }

        if ($count) {
            return $this->db->count_all_results();
        } else {
            $this->db->limit($limit, $offset);
            $queryy = $this->db->get();

            if ($queryy->num_rows() > 0) {
                return $queryy->result();
            } else {
                echo "

<tr><td colspan=\"5\"><b>Kayıt Bulunamadı!!!</b></td></tr>";
            }
        }

        return array();
    }

    public function get_count()
    {
        return $this->db->count_all("category");
    }

    public function get_records_supplier($limit, $count)
    {
        return $this->db->limit($limit, $count)->get("supplier")->result();
    }

    public function get_count_supplier()
    {
        return $this->db->count_all("supplier");
    }

    public function get_records_product($limit, $count)
    {
        return $this->db->limit($limit, $count)->get("product")->result();
    }

    public function get_count_product()
    {
        return $this->db->count_all("product");
    }

    public function get_records_purchase($limit, $count)
    {
        return $this->db->limit($limit, $count)->get("purchase")->result();
    }

    public function get_count_purchase()
    {
        return $this->db->count_all("purchase");
    }

    public function get_records_order($limit, $count)
    {
        return $this->db->limit($limit, $count)->get("order")->result();
    }

    public function get_count_order()
    {
        return $this->db->count_all("order");
    }

}
