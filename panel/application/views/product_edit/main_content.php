<!-- BEGIN CONTAINER -->
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form action="<?php echo base_url("product/edit/$result->id"); ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
        <div class="control-group">
            <label class="control-label">Ürün Kodu</label>
            <div class="controls">
                <input type="text" class="span6 " name="code" value="<?php echo $result->code; ?>"/>
                <span class="help-inline">Ürün Kodu Giriniz...</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Ürün Adı</label>
            <div class="controls">
                <input type="text" class="span6 " name="title" value="<?php echo $result->title; ?>"/>
                <span class="help-inline">Ürün Adı Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Detay</label>
            <div class="controls">
                <textarea name="detail" class="span6" cols="30" rows="10"><?php echo $result->detail; ?></textarea>
                <span class="help-inline">Tedarikçi Adresi giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Miktar</label>
            <div class="controls">
                <input type="text" class="span3 " name="quantity" value="<?php echo $result->quantity; ?>"/>
                <span class="help-inline">Miktar Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Alış Fiyatı</label>
            <div class="controls">
                <input type="text" class="span3 " name="list" value="<?php echo $result->list_price; ?>"/>
                <span class="help-inline">Alış Fiyatı Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Satış Fiyatı</label>
            <div class="controls">
                <input type="text" class="span3 " name="sale" value="<?php echo $result->sale_price; ?>"/>
                <span class="help-inline">Satış Fiyatı Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Kategori</label>
            <div class="controls">
                <select  name="category_id">
                    <?php foreach ($categories as $row) { ?>
                    <option <?php echo ($row->id==$category_id)?"selected":"" ?> value="<?php echo $row->id; ?>"><?php echo $row->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Tedarikçi</label>
            <div class="controls">
                <select name="supplier_id" >
                    <?php foreach ($supplier as $suppliers) { ?>
                        <option <?php echo ($suppliers->id==$supplier_id)?"selected":"" ?> value="<?php echo $suppliers->id; ?>"><?php echo $suppliers->title; ?></option>
                    <?php } ?>                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Resim</label>
            <div class="controls">
                <input type="hidden" name="old_photo" value="<?php echo $result->img_id ?>">
                <input type="file" class="span6" value="<?php echo $result->img_id ?>" name="img_id"/>
                <img src="<?php echo base_url("uploads/product/$result->img_id");  ?>" width="100" height="auto" alt="">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Aktif/Pasif</label>
            <div class="controls">
                <div id="normal-toggle-button">
                    <input name="isActive" type="checkbox" <?php echo ($result->isActive==1)?"checked":""; ?>>

                </div>
            </div>
        </div>

        <div class="form-actions no-padding" style="padding-top: 10px!important;">
            <button type="submit" class="btn btn-success">Kaydet</button>
            <a href="<?php echo base_url("product"); ?>" type="button" class="btn">İptal</a>
        </div>
    </form>
</div>
<!-- END PAGE CONTENT-->

<!-- END CONTAINER -->