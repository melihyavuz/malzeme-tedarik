<!-- BEGIN FOOTER -->

<div id="footer">
    <?php echo date("Y"); ?> &copy; Yavuz Dashboard.
</div>
<!-- END FOOTER -->


<?php
$this->load->view("includes/include_script");
$alert= $this->session->userdata("alert");

?>

<?php

 if($alert){
     $message=$this->session->userdata("alert-message");
     $type=$this->session->userdata("alert-type");?>

   <script>

    notif({
        msg: "<?php echo $message; ?>",
        type: "<?php echo $type;?>",
        position: "right",
        opacity:1,
        fade:true,
        width:"300"
    });
</script>

<?php
     $message=$this->session->set_userdata("alert",false);

 }?>
