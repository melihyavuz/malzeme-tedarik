<!-- BEGIN CONTAINER -->
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form action="<?php echo base_url("purchase/edit/$result->id"); ?>" class="form-horizontal" method="post">
        <div class="control-group">
            <label class="control-label">Fatura Numarası</label>
            <div class="controls">
                <input type="text" class="span6" value="<?php echo $result->invoice; ?>" name="invoice"/>
                <span class="help-inline">Fatura Numarası giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Tedarikçi</label>
            <div class="controls">
                <select name="supplier_id" id="supplier_id">
                    <?php foreach ($suppliers as $supplier) { ?>
                        <option <?php echo ($supplier->id == $result->supplier_id) ? "selected" : "" ?>
                                value="<?php echo $supplier->id; ?>"><?php echo $supplier->title; ?></option>
                    <?php } ?>                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Ürün Seçiniz</label>
            <div class="controls">
                <select name="product_id" id="product_id">
                    <?php foreach ($products as $product) { ?>
                        <option <?php echo ($product->id == $result->product_id) ? "selected" : ""; ?>
                                value="<?php echo $product->id; ?>"><?php echo $product->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Adet</label>
            <div class="controls">
                <input type="text" class="span3 " value="<?php echo $result->quantity; ?>" name="quantity"/>
                <span class="help-inline">Miktar Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Alış Fiyatı</label>
            <div class="controls">
                <input type="text" class="span3" value="<?php echo $result->price; ?>" name="price"/>
                <span class="help-inline">Alış Fiyatı Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Detay</label>
            <div class="controls">
                <textarea name="detail" class="span6" cols="30" rows="10"><?php echo $result->detail ?></textarea>
            </div>
        </div>
        <div class="form-actions no-padding" style="padding-top: 10px!important;">
            <button type="submit" class="btn btn-success">Kaydet</button>
            <a href="<?php echo base_url("product"); ?>" type="button" class="btn">İptal</a>
        </div>
    </form>
</div>
<!-- END PAGE CONTENT-->

<!-- END CONTAINER -->