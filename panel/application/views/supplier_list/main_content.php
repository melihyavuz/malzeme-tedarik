<!-- BEGIN BASIC PORTLET-->
<a href="<?php echo base_url("supplier/newPage"); ?>" class="btn btn-success"><i class="icon-plus"></i>Ekle</a>
<br><br>
<div class="row-fluid">
<?php if(sizeof($result)>0) { ?>
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
        <tr>
            <th><i class="icon-sort-by-order"></i> Kno</th>
            <th><i class="icon-bullhorn"></i> Tedarikçi</th>
            <th><i class="icon-bookmark"></i> Adres</th>
            <th><i class=" icon-phone"></i> Telefon</th>
            <th><i class=" icon-envelope"></i> Email</th>
            <th><i class=" icon-edit"></i> Durum</th>
            <th><i class=" icon-cogs"></i> İşlemler</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($results as $row) { ?>
            <tr>
                <td>#<?php echo $row->id; ?></td>
                <td><?php echo $row->title; ?></td>
                <td>
                   <?php
                   $address=$row->address;
                   $strlen=strlen($address);
                   if($strlen>40){
                      echo mb_substr($address,0,40)."...";
                   }
                   else{
                       echo $address;
                   }
                   ?>

                </td>
                <td><?php echo $row->phone; ?></td>
                <td>
                    <?php echo $row->email; ?>

                </td>
                <td>                   <input type="checkbox"
                                              name="my-checkbox"
                                              data-size="small"
                                              data-on-color="success"
                                              data-off-color="danger"
                                              data-on-text="Aktif"
                                              data-off-text="Pasif"
                                              dataURL="<?php echo base_url("supplier/isActiveSetter/"); ?>"
                                                  dataID="<?php echo $row->id; ?>"
                        <?php echo ($row->isActive) ? "checked" : "";
                        ?>
                    >
                </td>
                <td>
                    <a href="<?php echo base_url("supplier/updatePage/$row->id"); ?>" class="btn btn-primary"><i
                                class="icon-pencil"></i></a>
                    <a dataURL="<?php echo base_url("supplier/delete/$row->id"); ?>" class="btn btn-danger removeBtn"><i
                                class="icon-trash "></i></a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <p class="pagination"><?php echo $links; ?></p>

<?php } else { ?>
    <div class="alert alert-block alert-warning fade in">
        <h4 class="alert-heading"><strong>Dikkat!</strong></h4>
        <p>
            Maalesef herhangi bir tedarikçi bulunmamaktadır.Yeni tedarikçi eklemek için <a href="<?php echo base_url("supplier/newPage") ?>">tıklayınız</a>.
        </p>
    </div>
    <?php } ?>
</div>
<!-- END BASIC PORTLET-->

