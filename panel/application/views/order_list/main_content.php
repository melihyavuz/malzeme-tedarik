<!-- BEGIN BASIC PORTLET-->
<a href="<?php echo base_url("order/newPage"); ?>" class="btn btn-success"><i class="icon-plus"></i>Ekle</a>
<br><br>
<div class="row-fluid">
    <?php if (sizeof($results) > 0) { ?>
        <table class="table table-striped table-bordered table-advance table-hover">
            <thead>
            <tr>

                <th><i class="icon-sort-by-order"></i> Kno</th>
                <th><i class="icon-bullhorn"></i> Fatura Numarası</th>
                <th><i class="icon-bookmark"></i> Ürün</th>
                <th><i class=" icon-info"></i> Tedarikçi</th>
                <th><i class=" icon-shield"></i> Adet</th>
                <th><i class=" icon-money"></i> Satış Fiyatı</th>
                <th><i class=" icon-money"></i> Toplam Fiyat</th>
                <th><i class=" icon-money"></i> Tarih</th>
                <th><i class=" icon-cogs"></i> İşlemler</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($results as $row) { ?>
                <tr>


                    <td>#<?php echo $row->id; ?></td>
                    <td><?php echo $row->invoice; ?></td>
                    <td><?php echo get_product_title($row->product_id); ?></td>
                    <td><?php echo get_supplier_title($row->supplier_id); ?></td>
                    <td><?php echo $row->quantity; ?></td>
                    <td><?php echo $row->price; ?></td>
                    <td><?php echo $row->total_price; ?></td>
                    <td><?php echo $row->date; ?></td>

                    <td>
                        <a href="<?php echo base_url("order/updatePage/$row->id/$row->supplier_id"); ?>" class="btn btn-primary"><i
                                    class="icon-pencil"></i></a>
                        <a dataURL="<?php echo base_url("order/delete/$row->id"); ?>"
                           class="btn btn-danger removeBtn"><i
                                    class="icon-trash "></i></a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <p class="pagination"><?php echo $links ?></p>
    <?php } else { ?>
        <div class="alert alert-block alert-warning fade in">
            <h4 class="alert-heading"><strong>Dikkat!</strong></h4>
            <p>
                Maalesef herhangi bir alış faturası bulunmamaktadır.Yeni fatura eklemek için <a
                        href="<?php echo base_url("purchase/newPage") ?>">tıklayınız</a>.
            </p>
        </div>
    <?php } ?>

</div>
<!-- END BASIC PORTLET-->

