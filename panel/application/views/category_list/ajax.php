<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<script>

$(document).ready(function () {
    
    /*--first time load--*/
    ajaxproducts(page_url=false);


    $(document).on('click', ".pagination li a", function(event) {
        var page_url = $(this).attr('href');
        ajaxproducts(page_url);
        event.preventDefault();
    });

    /*-- create function ajaxlist --*/
    function ajaxproducts(page_url)
    {


        var base_url = '<?php echo base_url('category/index_ajax/') ?>';

        if(page_url == false) {
            var page_url = base_url;
        }

        $.ajax({
            type: "POST",
            url: page_url,
            data: {},
            success: function(response) {
                $("#ajaxContent").html(response);
            }
        });
    }
    function loadData(query){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url("category/index_ajax/") ?>",
            data: {query:query},
            success: function(response) {
                $("#ajaxContent").html(response);
            }
        });
    }
        $('#search_key').keyup(function () {
            var search=$(this).val();
            if(search!=''){
                loadData(search);
            }
            else{
                loadData();

            }
        });

});

</script>
