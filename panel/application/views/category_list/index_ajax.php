<!-- BEGIN CONTAINER -->
<!-- BEGIN PAGE CONTENT-->

<div style="clear:both"></div>
<div class="row-fluid" >
    <?php if (sizeof($categories) > 0) { ?>
        <table id="user_data"  class="table table-striped table-bordered table-advance table-hover">
            <thead>
            <tr>
                <th><i class="icon-bullhorn"></i> Başlık</th>
                <th><i class="icon-edit"></i> Durum</th>
                <th><i class=" icon-cogs"></i> İşlemler</th>
            </tr>
            </thead>

            <tbody>
            <?php if(!empty($categories)) { ?>

            <?php foreach ($categories as $category) { ?>
                <tr>
                    <td><?php echo $category->title; ?></td>
                    <td><input type="checkbox"
                               name="my-checkbox"
                               data-size="small"
                               data-on-color="success"
                               data-off-color="danger"
                               data-on-text="Aktif"
                               data-off-text="Pasif"
                               class="isActive"
                               dataURL="<?php echo base_url("category/isActiveSetter/"); ?>"

                               dataID="<?php echo $category->id; ?>"
                            <?php echo ($category->isActive) ? "checked" : "";
                            ?>
                        >
                    </td>
                    <td>
                        <a href="<?php echo base_url("category/editPage/$category->id"); ?>"
                           class="btn btn-primary"><i class="icon-pencil"></i></a>
                        <a dataURL="<?php echo base_url("category/delete/$category->id"); ?>"
                           class="btn btn-danger removeBtn"><i class="icon-trash "></i></a>
                    </td>
                </tr>

            <?php } ?>
            <?php } else { ?>
                <tr><td colspan="5"><?php echo $message ?></td></tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <div class="alert alert-block alert-warning fade in">
            <h4 class="alert-heading"><strong>Dikkat!</strong></h4>
            <p>
                Maalesef herhangi bir kategori bulunmamaktadır.Yeni kategori eklemek için <a
                    href="<?php echo base_url("category/newPage") ?>">tıklayınız</a>.
            </p>
        </div>
    <?php } ?>
</div>
<div class="box-footer">
    <ul class="pagination">
        <?php echo $pagelinks ?>
    </ul>
</div>
<style>
    .content {min-height:450px;}
    /*Pagination*/
    .pagination {
        float:right;
        list-style-type: none!important;
    }
    .pagination li a {
        padding:4px 8px;
        border:1px solid #0056b3;
        margin:2px;
    }
    .pagination li a:hover{
        text-decoration:none!important;
        background-color:#0056b3;
        color:#fff;
    }

    .pagination li a.current_page{
        background-color:#0056b3;
        color:#fff;
    }
</style>
<!-- END PAGE CONTENT-->
<script src="<?php echo base_url("assets/js/third_party/bootstrap-switch.min.js"); ?>"></script>

<script>
    $(document).ready(function () {
        $("[name='my-checkbox']").bootstrapSwitch();

    })

</script>
<script>
    $(document).ready(function () {
        $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch',function  (event,state) {
            console.log(state);
            var dataID=$(this).attr("dataID");
            var dataURL=$(this).attr("dataURL");

            $.post(
                dataURL,
                {isActive:state,id:dataID},
                function (data) {

                }

            )

        })
        $(".removeBtn").click(function () {
            var dataURL=$(this).attr("dataURL");
            var remove=confirm("Silmek istiyor musunuz?");
            if(remove==true){
                window.location.href=dataURL;
            }
        });

    });
</script>
<!-- END CONTAINER -->