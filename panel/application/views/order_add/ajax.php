<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<script>

    $(document).ready(function () {
        $('#supplier_id').change(function () {

            var supplier_id = $('#supplier_id').val();
            $.ajax({
                    url: '<?php echo base_url("order/fetch_product") ?>',
                method: "POST",
                data: { supplierId: supplier_id },
                dataType: "text",
                success: function (data) {
                    $('#product_id').html(data);

                }




            });


        });
        $('#product_id').change(function () {

            var product_id = $('#product_id').val();
            $.ajax({
                url: '<?php echo base_url("order/fetch_order") ?>',
                method: "POST",
                data: { productId: product_id },
                dataType: "text",
                success: function (data) {
                    $('#price').html(data);

                }




            });


        });

    });
</script>
