<!-- BEGIN CONTAINER -->
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form action="<?php echo base_url("order/save"); ?>" class="form-horizontal" method="post">
        <div class="control-group">
            <label class="control-label">Fatura Numarası</label>
            <div class="controls">
                <input type="text" class="span6 " name="invoice"/>
                <span class="help-inline">Fatura Numarası giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Tedarikçi</label>
            <div class="controls">
                <select  name="supplier_id" id="supplier_id">
                    <option value="">Lütfen Önce Tedarikçi Seçiniz...</option>
                    <?php foreach ($supplier as $suppliers) { ?>

                        <option value="<?php echo $suppliers->id; ?>"><?php echo $suppliers->title; ?></option>
                    <?php } ?>                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Ürün Seçiniz</label>
            <div class="controls">
                <select name="product_id" id="product_id">
                    <option value="">Lütfen Ürün Seçiniz...</option>

                    <?php foreach ($product as $row) { ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Adet</label>
            <div class="controls">
                <input type="text" class="span3 " name="quantity"/>
                <span class="help-inline">Miktar Giriniz...</span>
            </div>
        </div>
        <div class="control-group" id="price">
            <label class="control-label">Satış Fiyatı</label>
            <div class="controls" >
                <input type="text" class="span3 " name="price" id="pricee"/>
                <span class="help-inline">Satış Fiyatı Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Detay</label>
            <div class="controls">
                <textarea name="detail" class="span6" cols="30" rows="10"></textarea>
            </div>
        </div>
        <div class="form-actions no-padding" style="padding-top: 10px!important;">
            <button type="submit" class="btn btn-success">Kaydet</button>
            <a href="<?php echo base_url("order"); ?>" type="button" class="btn">İptal</a>
        </div>
    </form>
</div>
<!-- END PAGE CONTENT-->

<!-- END CONTAINER -->