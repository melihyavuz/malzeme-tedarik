<!-- BEGIN CONTAINER -->
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form action="<?php echo base_url("supplier/edit/$result->id"); ?>" class="form-horizontal" method="post">
        <div class="control-group">
            <label class="control-label">Tedarikçi Adı</label>
            <div class="controls">
                <input type="text" class="span6 " name="title" value="<?php echo $result->title; ?>"/>
                <span class="help-inline">Tedarikçi Adı giriniz...</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Tedarikçi Telefon</label>
            <div class="controls">
                <input type="text" class="span6 " name="phone" value="<?php echo $result->phone; ?>"/>
                <span class="help-inline">Tedarikçi Telefon giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Tedarikçi Mail</label>
            <div class="controls">
                <input type="text" class="span6 " name="mail" value="<?php echo $result->email; ?>"/>
                <span class="help-inline">Tedarikçi Mail giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Tedarikçi Adresi</label>
            <div class="controls">
                <textarea class="span6 " name="address"  cols="30" rows="10"><?php echo $result->address; ?></textarea>
                <span class="help-inline">Tedarikçi Adresi giriniz...</span>
            </div>
        </div>
            <div class="control-group">
                <label class="control-label">Aktif/Pasif</label>
                <div class="controls">
                    <div id="normal-toggle-button">
                        <input type="checkbox" name="isActive" <?php echo ($result->isActive==1)?"checked":"" ?> />                    </div>
                </div>
            </div>

        <div class="form-actions no-padding" style="padding-top: 10px!important;">
            <button type="submit" class="btn btn-success">Kaydet</button>
            <a href="<?php echo base_url("category"); ?>" type="button" class="btn">İptal</a>
        </div>
    </form>
</div>
<!-- END PAGE CONTENT-->

<!-- END CONTAINER -->