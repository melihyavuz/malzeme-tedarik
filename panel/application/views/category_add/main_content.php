<!-- BEGIN CONTAINER -->

<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form action="<?php echo base_url("category/save"); ?>" class="form-horizontal" method="post">
        <div class="control-group">
            <label class="control-label">Kategori Adı</label>
            <div class="controls">
                <input type="text" class="span6 " name="title" />
                <span class="help-inline">Kategori Adı giriniz...</span>
            </div>
        </div>

        <div class="form-actions no-padding" style="padding-top: 10px!important;">
            <button type="submit" class="btn btn-success">Kaydet</button>
            <a href="<?php echo base_url("category"); ?>" type="button" class="btn">İptal</a>
        </div>
    </form>
</div>
<!-- END PAGE CONTENT-->

<!-- END CONTAINER -->