<!-- BEGIN BASIC PORTLET-->
<a href="<?php echo base_url("product/newPage"); ?>" class="btn btn-success"><i class="icon-plus"></i>Ekle</a>
<br><br>
<div class="row-fluid">
<?php if(sizeof($results)>0) { ?>
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
        <tr>
            <th width="73">Ürün Resmi</th>

            <th><i class="icon-sort-by-order"></i> Kno</th>
            <th><i class="icon-bullhorn"></i> Kodu</th>
            <th><i class="icon-bookmark"></i> Ürün Adı</th>
            <th><i class=" icon-info"></i> Adet</th>
            <th><i class=" icon-money"></i> Alış Fiyatı</th>
            <th><i class=" icon-money"></i> Satış Fiyatı</th>
            <th><i class=" icon-shield"></i> Kategori</th>
            <th><i class=" icon-user"></i> Tedarikçi</th>
            <th><i class=" icon-edit"></i> Durum</th>

            <th><i class=" icon-cogs"></i> İşlemler</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($results as $row) { ?>
            <tr>
                <td width="60">
                    <?php
                    $image = FCPATH . "uploads/product/$row->img_id";
                    if (file_exists($image)) { ?>
                        <img src="<?php echo base_url("uploads/product/$row->img_id"); ?>"
                             alt="<?php echo $row->title; ?>">
                        <?php
                    } else {
                        ?>

                        <img src="<?php echo base_url("assets/img/product.jpg"); ?>" alt="şirket adı">

                    <?php } ?>
                </td>

                <td>#<?php echo $row->id; ?></td>
                <td><?php echo $row->code; ?></td>
                <td><?php echo $row->title; ?></td>

                <td><?php echo no_quantity($row->quantity); ?></td>
                <td><?php echo $row->list_price; ?></td>

                <td><?php echo $row->sale_price; ?></td>
                <td><?php echo get_category_title($row->category_id); ?></td>

                <td><?php echo get_supplier_title($row->supplier_id); ?></td>
                <td><input type="checkbox"
                           name="my-checkbox"
                           data-size="small"
                           data-on-color="success"
                           data-off-color="danger"
                           data-on-text="Aktif"
                           data-off-text="Pasif"
                           dataURL="<?php echo base_url("product/isActiveSetter/"); ?>"
                           dataID="<?php echo $row->id; ?>"
                        <?php echo ($row->isActive) ? "checked" : "";
                        ?>
                    >
                </td>
                <td>
                    <a href="<?php echo base_url("product/updatePage/$row->id"); ?>" class="btn btn-primary"><i
                                class="icon-pencil"></i></a>
                    <a dataURL="<?php echo base_url("product/delete/$row->id"); ?>" class="btn btn-danger removeBtn"><i
                                class="icon-trash "></i></a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <p class="pagination"><?php echo $links; ?></p>
    <?php } else { ?>
    <div class="alert alert-block alert-warning fade in">
        <h4 class="alert-heading"><strong>Dikkat!</strong></h4>
        <p>
            Maalesef herhangi bir ürün bulunmamaktadır.Yeni ürün eklemek için <a href="<?php echo base_url("product/newPage") ?>">tıklayınız</a>.
        </p>
    </div>
    <?php } ?>
</div>
<!-- END BASIC PORTLET-->

