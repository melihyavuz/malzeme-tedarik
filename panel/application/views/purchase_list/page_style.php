<style>
    .pagination a, .pagination strong {
        padding: 5px;
        margin-left: 5px;
        text-decoration: none;
        border: 1px solid #ccc;

    }

    .pagination strong {
        background-color: #35a5f2;
    }

    .pagination {
        display: flex;
        justify-content: flex-end;
    }
</style>
