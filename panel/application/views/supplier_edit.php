<!doctype html>
<html lang="tr">
<head>
    <?php $this->load->view("includes/head"); ?>
    <?php $this->load->view("category_edit/page_style"); ?>

    <title>index</title>
</head>
<body class="fixed-top">

<?php $this->load->view("includes/header"); ?>
<div id="container" class="row-fluid">
    <?php $this->load->view("includes/sidebar"); ?>
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <?php $this->load->view("supplier_edit/breadcrumb"); ?>
            <?php $this->load->view("supplier_edit/main_content"); ?>
        </div>
    </div>
</div>
<?php $this->load->view("includes/footer"); ?>
<?php $this->load->view("category_edit/page_script"); ?>

</body>
</html>