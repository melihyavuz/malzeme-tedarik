<!-- BEGIN CONTAINER -->
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form action="<?php echo base_url("product/save"); ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
        <div class="control-group">
            <label class="control-label">Ürün Kodu</label>
            <div class="controls">
                <input type="text" class="span6 " name="code"/>
                <span class="help-inline">Ürün Kodu Giriniz...</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Ürün Adı</label>
            <div class="controls">
                <input type="text" class="span6 " name="title"/>
                <span class="help-inline">Ürün Adı Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Detay</label>
            <div class="controls">
                <textarea name="address" class="span6" cols="30" rows="10"></textarea>
                <span class="help-inline">Detay</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Miktar</label>
            <div class="controls">
                <input type="text" class="span3 " name="quantity"/>
                <span class="help-inline">Miktar Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Alış Fiyatı</label>
            <div class="controls">
                <input type="text" class="span3 " name="list"/>
                <span class="help-inline">Alış Fiyatı Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Satış Fiyatı</label>
            <div class="controls">
                <input type="text" class="span3 " name="sale"/>
                <span class="help-inline">Satış Fiyatı Giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Kategori</label>
            <div class="controls">
                <select  name="category_id">
                    <?php foreach ($result as $row) { ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Tedarikçi</label>
            <div class="controls">
                <select name="supplier_id" >
                    <?php foreach ($supplier as $suppliers) { ?>
                        <option value="<?php echo $suppliers->id; ?>"><?php echo $suppliers->title; ?></option>
                    <?php } ?>                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Resim</label>
            <div class="controls">
                <input type="file" class="span6" name="img_id"/>
            </div>
        </div>

        <div class="form-actions no-padding" style="padding-top: 10px!important;">
            <button type="submit" class="btn btn-success">Kaydet</button>
            <a href="<?php echo base_url("product"); ?>" type="button" class="btn">İptal</a>
        </div>
    </form>
</div>
<!-- END PAGE CONTENT-->

<!-- END CONTAINER -->