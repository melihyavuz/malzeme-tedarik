<!-- BEGIN CONTAINER -->

<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form action="<?php echo base_url("supplier/save"); ?>" class="form-horizontal" method="post">
        <div class="control-group">
            <label class="control-label">Tedarikçi Adı</label>
            <div class="controls">
                <input type="text" class="span6 " name="title"/>
                <span class="help-inline">Tedarikçi Adı giriniz...</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Tedarikçi Telefon</label>
            <div class="controls">
                <input type="text" class="span6 " name="phone"/>
                <span class="help-inline">Tedarikçi Telefon giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Tedarikçi Mail</label>
            <div class="controls">
                <input type="text" class="span6 " name="mail"/>
                <span class="help-inline">Tedarikçi Mail giriniz...</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Tedarikçi Adresi</label>
            <div class="controls">
                <textarea name="address" class="span6" cols="30" rows="10"></textarea>
                <span class="help-inline">Tedarikçi Adresi giriniz...</span>
            </div>
        </div>

        <div class="form-actions no-padding" style="padding-top: 10px!important;">
            <button type="submit" class="btn btn-success">Kaydet</button>
            <a href="<?php echo base_url("supplier"); ?>" type="button" class="btn">İptal</a>
        </div>
    </form>
</div>
<!-- END PAGE CONTENT-->

<!-- END CONTAINER -->