<?php

function getMessage($key)
{
    $messages = array(
        "success" => "İşlem Başarılıdır",
        "error" => "İşlem Başarısızdır",
        "blankField" => "Lütfen Tüm Alanları Doldurun",
        "errorFileUpload" => "Resim Yüklerken hata oluştu",
        "noQuantity" => "Bu kadar adet ürün yoktur",
        "notDeletedCategory" => "Bu kategori bir ürüne bağlıdır.Silemezsiniz.",
        "notSupplierDelete" => "Bu tedarikçi bir ürüne bağlıdır.Silemezsiniz."
    );
    return $messages[$key];
}
